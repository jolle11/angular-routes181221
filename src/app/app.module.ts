import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AboutPageComponent } from './pages/about-page/about-page.component';
import { AboutDetailPageComponent } from './pages/about-page/about-detail-page/about-detail-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';

import { FooterComponent } from './shared/components/footer/footer.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { TeacherCardComponent } from './shared/components/teacher-card/teacher-card.component';
import { CopyrightComponent } from './shared/components/footer/copyright/copyright.component';

@NgModule({
    declarations: [
        AppComponent,
        HomePageComponent,
        HeaderComponent,
        FooterComponent,
        LoginPageComponent,
        AboutPageComponent,
        ContactPageComponent,
        AboutDetailPageComponent,
        TeacherCardComponent,
        CopyrightComponent,
    ],
    imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
