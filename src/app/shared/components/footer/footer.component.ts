import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
    currentYear: number = 2021;
    social = ['instagram', 'facebook', 'twitter', 'pinterest'];
    addSocial(socialOutput: string) {
        this.social.push(socialOutput);
    }

    constructor() {}

    ngOnInit(): void {}
}
