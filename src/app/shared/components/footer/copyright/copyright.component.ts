import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-copyright',
    templateUrl: './copyright.component.html',
    styleUrls: ['./copyright.component.scss'],
})
export class CopyrightComponent implements OnInit {
    @Input() year?: number;
    @Output() socialOutput = new EventEmitter<string>();
    addSocial(value: string): void {
        this.socialOutput.emit(value);
    }
    constructor() {}

    ngOnInit(): void {}
}
