import { Component, Input, OnInit } from '@angular/core';
import { Teacher } from 'src/app/core/models/Iteacher';

@Component({
    selector: 'app-teacher-card',
    templateUrl: './teacher-card.component.html',
    styleUrls: ['./teacher-card.component.scss'],
})
export class TeacherCardComponent implements OnInit {
    teachers: Teacher = {
        id: 1,
        image: 'https://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png',
        name: 'Jordi',
        subject: 'Full Stack Development',
        students: 20,
    };

    constructor() {}

    ngOnInit(): void {}
}
