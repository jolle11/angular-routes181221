export interface Teacher {
    id: number;
    image: string;
    name: string;
    subject: string;
    students: number;
}
