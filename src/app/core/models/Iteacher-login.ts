export interface TeacherLogin {
    id: number;
    name: string;
    password: string;
}
