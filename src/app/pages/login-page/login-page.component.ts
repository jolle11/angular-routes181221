import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TeacherLogin } from '../../core/models/Iteacher-login';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
    // Form initialisation
    public loginForm: FormGroup = new FormGroup({});
    // Variable for the conditio message
    public submitted: boolean = false;
    constructor(private formBuilder: FormBuilder) {}

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            id: ['', [Validators.required, Validators.pattern(/^[0-9]{1,5}$/)]],
            name: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z]).{6,}$/)]],
            password: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{10,}$/)]],
        });
    }
    onSubmit(): void {
        this.submitted = true;
        // When form is validated
        if (this.loginForm.valid) {
            setTimeout(() => {
                const teacher: TeacherLogin = {
                    id: this.loginForm.get('id')?.value,
                    name: this.loginForm.get('name')?.value,
                    password: this.loginForm.get('password')?.value,
                };
                this.loginForm.reset();
                this.submitted = false;
            }, 3000);
        }
    }
}
