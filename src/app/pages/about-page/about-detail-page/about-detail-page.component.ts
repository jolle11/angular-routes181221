import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { globalTeachers } from '../about-page.component';

@Component({
    selector: 'app-about-detail-page',
    templateUrl: './about-detail-page.component.html',
    styleUrls: ['./about-detail-page.component.scss'],
})
export class AboutDetailPageComponent implements OnInit {
    teacherDetail: any = {};
    params$: any;
    constructor(private route: ActivatedRoute) {}

    ngOnInit(): void {
        this.params$ = this.route.paramMap.subscribe((params) => {
            const teacherId = params.get('teacherID');
            this.teacherDetail = globalTeachers.find((teacher) => teacher.id.toString() === teacherId);
        });
    }
    ngOnDestroy() {
        this.params$.unsubscribe();
    }
}
