import { Component, OnInit } from '@angular/core';

export const globalTeachers = [
    {
        id: 1,
        image: 'https://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png',
        name: 'Jordi',
        subject: 'Full Stack Development',
        students: 20,
    },
    {
        id: 2,
        image: 'https://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png',
        name: 'Cristina',
        subject: 'Full Stack Development',
        students: 30,
    },
    {
        id: 3,
        image: 'https://icons.iconarchive.com/icons/papirus-team/papirus-status/256/avatar-default-icon.png',
        name: 'Joe',
        subject: 'Full Stack Development',
        students: 40,
    },
];

@Component({
    selector: 'app-about-page',
    templateUrl: './about-page.component.html',
    styleUrls: ['./about-page.component.scss'],
})
export class AboutPageComponent implements OnInit {
    teachers = globalTeachers;
    constructor() {}

    ngOnInit(): void {}
}
